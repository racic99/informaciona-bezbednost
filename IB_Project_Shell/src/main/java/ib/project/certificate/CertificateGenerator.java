package ib.project.certificate;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import ib.project.model.IssuerData;
import ib.project.model.SubjectData;

public class CertificateGenerator {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	 
	public X509Certificate generateCertificate(IssuerData issuerData, SubjectData subjectData) {
		try {

			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
			
			builder = builder.setProvider("BC");
			
			ContentSigner contentSigner = builder.build(issuerData.getPrivateKey());
			
			X509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(
					issuerData.getX500name(),
					new BigInteger(subjectData.getSerialNumber()),
					subjectData.getStartDate(),
					subjectData.getEndDate(),
					subjectData.getX500name(),
					subjectData.getPublicKey());
			
			X509CertificateHolder certHolder = certBuilder.build(contentSigner);
			
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
			
			certConverter = certConverter.setProvider("BC");

			return certConverter.getCertificate(certHolder);

		} catch (IllegalArgumentException | IllegalStateException | OperatorCreationException | CertificateException e){
			e.printStackTrace();
		}
		
		return null;
	}
	 
	public KeyPair generateKeyPair() {
		try {

			KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
			
			keyGenerator.initialize(1024);
			
			KeyPair keyPair = keyGenerator.generateKeyPair();
			return keyPair;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	 
	public X509Certificate generateSelfSignedCertificate(KeyPair keyPair, String email) {
		
		SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		Date endDate = new Date();
		try {
			startDate = iso8601Formater.parse("2020-04-01");
			endDate = iso8601Formater.parse("2025-04-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
		builder.addRDN(BCStyle.CN, "Filip Racic");
		builder.addRDN(BCStyle.SURNAME, "Racic");
		builder.addRDN(BCStyle.GIVENNAME, "Filip");
		builder.addRDN(BCStyle.O, "FTN");
		builder.addRDN(BCStyle.OU, "Informaciona Bezbednost");
		builder.addRDN(BCStyle.C, "RS");
		builder.addRDN(BCStyle.E, email);
		builder.addRDN(BCStyle.UID, "12345");

		String serialNumber = "1";

		X500Name x500Name = builder.build();
		
		IssuerData issuerData = new IssuerData(keyPair.getPrivate(), x500Name);
		
		SubjectData subjectData = new SubjectData(keyPair.getPublic(), x500Name, serialNumber, startDate, endDate);
		
		return generateCertificate(issuerData, subjectData);
	}
	 
	public void printCertificate(X509Certificate cert) {
		System.out.println("ISSUER: " + cert.getIssuerX500Principal().getName());
		System.out.println("SUBJECT: " + cert.getSubjectX500Principal().getName());
		System.out.println("Sertifikat:");
		System.out.println("--------------------------------------------------------------------------------------------------------------");
		System.out.println(cert);
		System.out.println("--------------------------------------------------------------------------------------------------------------");
		System.out.println("--------------------------------------------------------------------------------------------------------------");
	}
	
}

