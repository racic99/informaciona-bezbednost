package ib.project.keystore;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class KeyStoreReader {
	 
	public KeyStore readKeyStore(String keyStorePutanja, char[] password) {
		KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance("JKS", "SUN");
			
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStorePutanja));
			keyStore.load(in, password);
		}catch (KeyStoreException | NoSuchProviderException | NoSuchAlgorithmException | CertificateException | IOException e) {
			e.printStackTrace();
			System.err.println("\nGreska prilikom ucitavanja KeyStore-a!\n");			
		}
		
		return keyStore;
	}
	
	public Certificate getCertificateFromKeyStore(KeyStore keyStore, String alias) {
		Certificate cert = null;
		try {
			cert = keyStore.getCertificate(alias);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		
		if (cert == null) {
			System.err.println("\nSertifikat je null. Proveriti ispravnost aliasa!\n");
		}
		
		return cert;
	}

	public PrivateKey getPrivateKeyFromKeyStore(KeyStore keyStore, String alias, char[] keyPass) {
		PrivateKey privateKey = null;
		try {
			privateKey = (PrivateKey) keyStore.getKey(alias, keyPass);
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		if (privateKey == null) {
			System.err.println("\nPrivatni kljuc je null!\n");
		}
		
		return privateKey;
	}

	public PublicKey getPublicKeyFromCertificate(Certificate cert) {
		return cert.getPublicKey();
	}
	
}

