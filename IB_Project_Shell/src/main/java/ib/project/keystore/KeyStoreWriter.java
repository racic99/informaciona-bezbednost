package ib.project.keystore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class KeyStoreWriter {
	  
	public KeyStore loadKeyStore(String keyStorePutanja, char[] password) {
		KeyStore keyStore = null;
		try {
 
			keyStore = KeyStore.getInstance("JKS");
			
			if (keyStorePutanja != null)  {
				keyStore.load(new FileInputStream(keyStorePutanja), password);
			}else {
				keyStore.load(null, password);
			}
		}catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
			e.printStackTrace();
			System.err.println("\nGreska prilikom ucitavanja KeyStore-a!\n");			
		}
		return keyStore;
	}
	
	 
	public void saveKeyStore(KeyStore keyStore, String keyStorePutanja, char[] password) {
		try {
			keyStore.store(new FileOutputStream(keyStorePutanja), password);
		} catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
			e.printStackTrace();
			System.err.println("\nGreska prilikom snimanja KeyStore-a!\n");			
		}
	}
	
	 
	public void addToKeyStore(KeyStore keyStore, String alias, PrivateKey privateKey, char[] password, Certificate cert) {
		try {
			keyStore.setKeyEntry(alias, privateKey, password, new Certificate[] {cert});
		} catch (KeyStoreException e) {
			e.printStackTrace();
			System.err.println("\nGreska prilikom snimanja KeyStore-a!\n");			
		}
	}
	
}

