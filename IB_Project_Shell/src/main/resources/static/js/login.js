$(document).ready(function(){

	var errorMessage = $('#errorMessage');
	errorMessage.hide();
	
	$('#loginSubmit').on('click', function(event) {
		var emailInput = $('#emailInput');
		var passwordInput = $('#passwordInput');
		
		var email = emailInput.val();
		var password = passwordInput.val();
		
		if(email == '' || passwordInput == ''){
			errorMessage.show();
			errorMessage[0].innerHTML = "Sva polja moraju biti popunjena";
			setTimeout(function () {
				errorMessage.hide();
			}, 3000);
    		event.preventDefault();
            return false;
        }
		
		$.post('api/users/user/login', {'email': email, 'password': password}, function(response) {
			if (response == '') {
				errorMessage.show();
				errorMessage[0].innerHTML = "Pogresni podaci";
				setTimeout(function () {
					errorMessage.hide();
				}, 3000);
				event.preventDefault();
				return;
			} 
			if(response != '') {
				var userEmail = response.email;
				sessionStorage.setItem('userEmail', userEmail);
				if(response.authority.name == 'Admin'){
					window.location.href = 'admin.html';
				}else {
					window.location.href = 'user.html';
				}
			}
		});
		event.preventDefault();
		return false;
	});
});