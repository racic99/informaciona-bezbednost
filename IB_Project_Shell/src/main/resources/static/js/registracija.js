$(document).ready(function(){

    var errorMessage = $('#errorMessage');
	errorMessage.hide();
	
	$('#registerSubmit').on('click', function(event) {
		var emailInput = $('#emailInput');
		var passwordInput = $('#passwordInput');
		var passwordInputRepeat = $('#passwordInputRepeat');
		
		var email = emailInput.val();
		var password = passwordInput.val();
		var passwordRepeat = passwordInputRepeat.val();
		
		if(email == '' || password == '' || passwordRepeat == ''){
			errorMessage.show();
			errorMessage[0].innerHTML = "Sva polja moraju biti popunjena";
			setTimeout(function () {
				errorMessage.hide();
			}, 3000);
    		event.preventDefault();
            return false;
        }

        console.log(password);
        console.log(passwordRepeat)

		if(password != passwordRepeat){
			errorMessage.show();
			errorMessage[0].innerHTML = "Lozinke se ne poklapaju";
			setTimeout(function () {
				errorMessage.hide();
			}, 3000);
    		event.preventDefault();
            return false;
        }

		$.post('api/users/user/registration', {'email': email, 'password': password}, function(response){
            if (response == '') {
				errorMessage.show();
				errorMessage[0].innerHTML = "Došlo je do greške";
				setTimeout(function () {
					errorMessage.hide();
				}, 3000);
				event.preventDefault();
				return;
			} 
			if(response != '') {
                window.location.replace("login.html");
			}
		});
        
        event.preventDefault();
		return false;
	});
});