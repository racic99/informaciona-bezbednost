package signature;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SignEnveloped {
	
	static {

		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}
	
	public static void testIt(String senderEmail) {
		String inFile = "./data/" + senderEmail + ".xml";
		String outFile = "./data/" + senderEmail + "_signed.xml";
		
		Document doc = loadDocument(inFile);

		PrivateKey pk = readPrivateKey(senderEmail);

		Certificate cert = readCertificate(senderEmail);

		System.out.println("Potpisivanje....");
		doc = signDocument(doc, pk, cert);

		saveDocument(doc, outFile);
		System.out.println("Potpisivanje dokumenta zavrseno");
	}

	private static Document loadDocument(String file) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new File(file));

			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	 
	private static void saveDocument(Document doc, String fileName) {
		try {
			File outFile = new File(fileName);
			FileOutputStream f = new FileOutputStream(outFile);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);
			
			transformer.transform(source, result);

			f.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Certificate readCertificate(String email) {
		try {
			String keyStoreFile = "./data/" + email +".jks";
			
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");
			
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStoreFile));
			ks.load(in, "123".toCharArray());
			
			if(ks.isKeyEntry(email)) {
				Certificate cert = ks.getCertificate(email);
				return cert;
				
			}
			else
				return null;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	
	private static PrivateKey readPrivateKey(String email) {
		try {
			String keyStoreFile = "./data/" + email +".jks";

			KeyStore ks = KeyStore.getInstance("JKS", "SUN");

			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStoreFile));
			ks.load(in, "123".toCharArray());
			
			if(ks.isKeyEntry(email)) {
				PrivateKey pk = (PrivateKey) ks.getKey(email, "123".toCharArray());
				return pk;
			}
			else
				return null;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static Document signDocument(Document doc, PrivateKey privateKey, Certificate cert) {
      
      try {
			Element rootEl = doc.getDocumentElement();

			XMLSignature sig = new XMLSignature(doc, null, XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA1);

			Transforms transforms = new Transforms(doc);

			transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);

			transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);

			sig.addDocument("", transforms, Constants.ALGO_ID_DIGEST_SHA1);

			sig.addKeyInfo(cert.getPublicKey());
			sig.addKeyInfo((X509Certificate) cert);

			rootEl.appendChild(sig.getElement());

			sig.sign(privateKey);
			
			return doc;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
