package app;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.Security;

import javax.mail.internet.MimeMessage;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.google.api.services.gmail.Gmail;

import signature.SignEnveloped;
import xml.AsymmetricKeyEncryption;
import xml.CreateXMLDOM;
import support.MailHelper;
import support.MailWritter;

public class WriteMailClient extends MailClient {
	
	static {
		
		Security.addProvider(new BouncyCastleProvider());
		org.apache.xml.security.Init.init();
	}
	
	public static void main(String[] args) {
		
        try {
        	Gmail service = getGmailService();
        	
        	System.out.println("Unesi email posiljaoca:");
        	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String userEmail = reader.readLine();
            final String xmlFilePath = "./data/" + userEmail + "_enc.xml";
            
        	System.out.println("Unesi email primaoca:");
            String reciever = reader.readLine();
        	
            System.out.println("Unesi naslov:");
            String subject = reader.readLine();
            
            System.out.println("Unesi poruku:");
            String body = reader.readLine();
            
            CreateXMLDOM.createXML(userEmail, subject, body);
            
        	SignEnveloped.testIt(userEmail);
            
            AsymmetricKeyEncryption.testIt(userEmail, reciever);
            
            MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, xmlFilePath);
            MailWritter.sendMessage(service, "me", mimeMessage);
        	
        }catch (Exception e) {
        	e.printStackTrace();
		}
	}
}
