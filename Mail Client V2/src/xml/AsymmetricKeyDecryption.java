package xml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.encryption.XMLCipher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AsymmetricKeyDecryption {

	public static void testIt(String senderEmail, String recieverEmail) {
		String inFile = "./data/" + senderEmail + "_enc.xml";
		String outFile = "./data/" + senderEmail + "_dec.xml";
		
		Document doc = loadDocument(inFile);

		PrivateKey pk = readPrivateKey(recieverEmail);

		System.out.println("Dekripcija....");
		doc = decrypt(doc, pk);

		saveDocument(doc, outFile);
		System.out.println("Dekripcija zavrsena\n");
	}

	private static Document loadDocument(String file) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new File(file));

			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void saveDocument(Document doc, String fileName) {
		try {
			File outFile = new File(fileName);
			FileOutputStream f = new FileOutputStream(outFile);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);

			transformer.transform(source, result);

			f.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static PrivateKey readPrivateKey(String email) {
		try {
			String keyStorePutanja = "./data/" + email +".jks";
			
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");

			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStorePutanja));
			ks.load(in, "123".toCharArray());

			if (ks.isKeyEntry(email)) {
				PrivateKey pk = (PrivateKey) ks.getKey(email, "123".toCharArray());
				return pk;
			} else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Document decrypt(Document doc, PrivateKey privateKey) {

		try {

			XMLCipher xmlCipher = XMLCipher.getInstance();

			xmlCipher.init(XMLCipher.DECRYPT_MODE, null);

			xmlCipher.setKEK(privateKey);

			NodeList encDataList = doc.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedData");
			Element encData = (Element) encDataList.item(0);

			xmlCipher.doFinal(doc, encData);

			return doc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
