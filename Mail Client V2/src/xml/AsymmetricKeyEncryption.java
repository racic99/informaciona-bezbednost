package xml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.encryption.EncryptedData;
import org.apache.xml.security.encryption.EncryptedKey;
import org.apache.xml.security.encryption.XMLCipher;
import org.apache.xml.security.keys.KeyInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AsymmetricKeyEncryption {

	public static void testIt(String senderEmail, String recieverEmail) {
		String inFile = "./data/" + senderEmail + "_signed.xml";
		String outFile = "./data/" + senderEmail + "_enc.xml";

		Document doc = loadDocument(inFile);

		System.out.println("Generisanje tajnog kljuca ....");
		SecretKey secretKey = generateDataEncryptionKey();
		
		Certificate cert = readCertificate(recieverEmail);

		System.out.println("Enkripcija....");
		doc = encrypt(doc, secretKey, cert);

		saveDocument(doc, outFile);
		
		System.out.println("Enkripcija zavrsena");
	}
	public static Document loadDocument(String file) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new File(file));

			return document;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}

	private static Certificate readCertificate(String email) {
		try {
			String keyStorePutanja = "./data/" + email +".jks";
			
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");

			BufferedInputStream in = new BufferedInputStream(new FileInputStream(keyStorePutanja));
			ks.load(in, "123".toCharArray());

			if (ks.isKeyEntry(email)) {
				Certificate cert = ks.getCertificate(email);
				return cert;
			} else
				return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}

	private static void saveDocument(Document doc, String fileName) {
		try {
			File outFile = new File(fileName);
			FileOutputStream f = new FileOutputStream(outFile);

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);

			transformer.transform(source, result);

			f.close();

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	private static SecretKey generateDataEncryptionKey() {

		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");
			
			return keyGenerator.generateKey();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Document encrypt(Document doc, SecretKey key, Certificate certificate) {

		try {

			XMLCipher xmlCipher = XMLCipher.getInstance(XMLCipher.TRIPLEDES);
			
			xmlCipher.init(XMLCipher.ENCRYPT_MODE, key);

			XMLCipher keyCipher = XMLCipher.getInstance(XMLCipher.RSA_v1dot5);
			
			keyCipher.init(XMLCipher.WRAP_MODE, certificate.getPublicKey());
			
			EncryptedKey encryptedKey = keyCipher.encryptKey(doc, key);

			EncryptedData encryptedData = xmlCipher.getEncryptedData();

			KeyInfo keyInfo = new KeyInfo(doc);

			keyInfo.addKeyName("Kriptovani tajni kljuc");

			keyInfo.add(encryptedKey);

			encryptedData.setKeyInfo(keyInfo);

			NodeList emailList = doc.getElementsByTagName("email");
			Element email = (Element) emailList.item(0);

			xmlCipher.doFinal(doc, doc.getDocumentElement(), true);

			return doc;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	
}

